﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Xml.Linq;
using TugasEAITika.Models;

namespace TugasEAITika.Services
{
    [ServiceContract]
    public class PraktikumService
    {
        private readonly ILogger<PraktikumService> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _connstring;

        public PraktikumService(ILogger<PraktikumService> logger, IConfiguration Configuration)
        {
            _logger = logger;
            _configuration = Configuration;
            _connstring = _configuration.GetConnectionString("DefaultDatabase");
        }

        [OperationContract]
        public string Speed(string s)
        {
            Console.WriteLine(s + " MPH");
            return s;
        }

        [OperationContract]
        public List<Praktikum> ListPraktikum()
        {
            List<Praktikum> res = new List<Praktikum>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_praktikum rp;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    // Here you get the object
                    res = (List<Praktikum>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Praktikum>));
                }

            }
            catch (Exception)
            {
            }
            finally
            {
                conSQL.Close();
            }
            return res;
        }

        [OperationContract]
        public Praktikum DetailPraktikum(int id)
        {
            List<Praktikum> res = new List<Praktikum>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_praktikum rk where id = @id;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.Add(new SqlParameter("@id", id));
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    res = (List<Praktikum>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Praktikum>));
                }
            }
            catch
            {

            }
            finally
            {
                conSQL.Close();
            }
            return res[0];
        }

        [OperationContract]
        public string CreatePraktikum(Praktikum data)
        {
            string res = "";
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                string sql = @"INSERT INTO reservasi_praktikum (nama_lengkap,nim,nomor_telpon,email,nama_penanggung_jawab,laboratorium,mata_kuliah,nama_modul,ruangan,waktu_mulai,waktu_selesai,satuan_waktu,berapa_kali,tanggal_peminjaman)
	VALUES (@nama_lengkap,@nim,@nomor_telpon,@email,@nama_penanggung_jawab,@laboratorium,@mata_kuliah,@nama_modul,@ruangan,@waktu_mulai,@waktu_selesai,@satuan_waktu,@berapa_kali,@tanggal_peminjaman);";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.AddWithValue("@nama_lengkap", data.nama_lengkap);
                command.Parameters.AddWithValue("@nim", data.nim);
                command.Parameters.AddWithValue("@nomor_telpon", data.nomor_telpon);
                command.Parameters.AddWithValue("@email", data.email);
                command.Parameters.AddWithValue("@nama_penanggung_jawab", data.nama_penanggung_jawab);
                command.Parameters.AddWithValue("@laboratorium", data.laboratorium);
                command.Parameters.AddWithValue("@mata_kuliah", data.mata_kuliah);
                command.Parameters.AddWithValue("@nama_modul", data.nama_modul);
                command.Parameters.AddWithValue("@ruangan", data.ruangan);
                command.Parameters.AddWithValue("@waktu_mulai", data.waktu_mulai);
                command.Parameters.AddWithValue("@waktu_selesai", data.waktu_selesai);
                command.Parameters.AddWithValue("@satuan_waktu", data.satuan_waktu);
                command.Parameters.AddWithValue("@berapa_kali", data.berapa_kali);
                command.Parameters.AddWithValue("@tanggal_peminjaman", data.tanggal_peminjaman);
                conSQL.Open();
                command.ExecuteNonQuery();
                res = "Success";
            }
            catch (Exception e)
            {
                res = e.Message;
            }
            finally
            {
                conSQL.Close();
            }
            return res;
        }

        [OperationContract]
        public void XmlMethod(XElement xml)
        {
            Console.WriteLine(xml.ToString());
        }
    }
}
