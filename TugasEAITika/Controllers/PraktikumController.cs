﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TugasEAITika.Controllers
{
    public class PraktikumController : Controller
    {
        // GET: PraktikumController
        public ActionResult Index()
        {
            return View();
        }

        // GET: PraktikumController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PraktikumController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PraktikumController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PraktikumController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PraktikumController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PraktikumController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PraktikumController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
