﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TugasEAITika.Models;

namespace TugasEAITika.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _connstring;

        public HomeController(ILogger<HomeController> logger, IConfiguration Configuration)
        {
            _logger = logger;
            _configuration = Configuration;
            _connstring = _configuration.GetConnectionString("DefaultDatabase");
        }

        public IActionResult Index()
        {
            List<Kelas> list_kelas = new List<Kelas>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_kelas rk ;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    // Here you get the object
                    list_kelas = (List<Kelas>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Kelas>));
                }

            }
            catch (Exception)
            {
                return Ok();
            }
            finally
            {
                conSQL.Close();
            }

            List<Praktikum> list_praktikum = new List<Praktikum>();
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_praktikum rp;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    // Here you get the object
                    list_praktikum = (List<Praktikum>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Praktikum>));
                }

            }
            catch (Exception)
            {
            }
            finally
            {
                conSQL.Close();
            }
            return View(new IndexData { Kelas = list_kelas});
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
