﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TugasEAITika.Models;

namespace TugasEAITika.Controllers
{
    public class KelasController : Controller
    {
        private readonly ILogger<RestController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _connstring;

        public KelasController(ILogger<RestController> logger, IConfiguration Configuration)
        {
            _logger = logger;
            _configuration = Configuration;
            _connstring = _configuration.GetConnectionString("DefaultDatabase");
        }
        // GET: KelasController
        public ActionResult Index()
        {
            return View();
        }

        // GET: KelasController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: KelasController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KelasController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: KelasController/Edit/5
        public ActionResult Edit(int id)
        {
            List<Kelas> res = new List<Kelas>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_kelas rk where id = @id;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.Add(new SqlParameter("@id", id));
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    res = (List<Kelas>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Kelas>));
                    DateTime tanggal = DateTime.Parse(res[0].tanggal_peminjaman);
                    res[0].tanggal_peminjaman = tanggal.ToString("yyy-MM-dd");
                }
            }
            catch
            {

            }
            finally
            {
                conSQL.Close();
            }
            return View(res[0]);
        }

        // POST: KelasController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: KelasController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: KelasController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
