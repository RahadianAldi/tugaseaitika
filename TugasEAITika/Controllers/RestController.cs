﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TugasEAITika.Models;

namespace TugasEAITika.Controllers
{
    [Route("rest/kelas")]
    [ApiController]
    public class RestController : Controller
    {
        private readonly ILogger<RestController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string _connstring;

        public RestController(ILogger<RestController> logger, IConfiguration Configuration)
        {
            _logger = logger;
            _configuration = Configuration;
            _connstring = _configuration.GetConnectionString("DefaultDatabase");
        }

        // GET: RuanganController
        //[HttpGet("index")]
        public ActionResult Index()
        {
            List<Kelas> res = new List<Kelas>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_kelas rk ;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    // Here you get the object
                    res = (List<Kelas>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Kelas>));
                }

            }
            catch (Exception)
            {
                return Ok();
            }
            finally
            {
                conSQL.Close();
            }
            return Ok(res);
        }

        [HttpGet("details/{id}")]
        // GET: RuanganController/Details/5
        public ActionResult Details(int id)
        {
            List<Kelas> res = new List<Kelas>();
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                conSQL.Open();
                string sql = @"select * from reservasi_kelas rk where id = @id;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.Add(new SqlParameter("@id", id));
                SqlDataReader dtr = command.ExecuteReader();
                var datatable = new DataTable();
                datatable.Load(dtr);
                if (datatable.Rows.Count > 0)
                {
                    var serializedMyObjects = JsonConvert.SerializeObject(datatable);
                    res = (List<Kelas>)JsonConvert.DeserializeObject(serializedMyObjects, typeof(List<Kelas>));
                }
            }
            catch
            {

            }
            finally
            {
                conSQL.Close();
            }
            return Ok(res[0]);
        }

        // GET: RuanganController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: RuanganController/Create
        [HttpPost("create")]
        public ActionResult Create([FromBody] Kelas data)
        {
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            { 
                string sql = @"insert into reservasi_kelas (nama_lengkap, nim, nomor_telpon, email, organisasi, jabatan, nama_penanggung_jawab, ruangan, waktu_mulai, waktu_selesai, satuan_waktu, berapa_kali, nama_acara, kategori_acara, deskripsi_acara, tanggal_peminjaman) values (@nama_lengkap, @nim, @nomor_telpon, @email, @organisasi, @jabatan, @nama_penanggung_jawab, @ruangan, @waktu_mulai, @waktu_selesai, @satuan_waktu, @berapa_kali, @nama_acara, @kategori_acara, @deskripsi_acara, @tanggal_peminjaman);";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.AddWithValue("@nama_lengkap", data.nama_lengkap);             
                command.Parameters.AddWithValue("@nim", data.nim);             
                command.Parameters.AddWithValue("@email", data.email);             
                command.Parameters.AddWithValue("@nomor_telpon", data.nomor_telpon);             
                command.Parameters.AddWithValue("@organisasi", data.organisasi);             
                command.Parameters.AddWithValue("@jabatan", data.jabatan);             
                command.Parameters.AddWithValue("@nama_penanggung_jawab", data.nama_penanggung_jawab);             
                command.Parameters.AddWithValue("@ruangan", data.ruangan);             
                command.Parameters.AddWithValue("@waktu_mulai", data.waktu_mulai);             
                command.Parameters.AddWithValue("@waktu_selesai", data.waktu_selesai);             
                command.Parameters.AddWithValue("@satuan_waktu", data.satuan_waktu);             
                command.Parameters.AddWithValue("@berapa_kali", data.berapa_kali);             
                command.Parameters.AddWithValue("@nama_acara", data.nama_acara);             
                command.Parameters.AddWithValue("@kategori_acara", data.kategori_acara);             
                command.Parameters.AddWithValue("@deskripsi_acara", data.deskripsi_acara);             
                command.Parameters.AddWithValue("@tanggal_peminjaman", data.tanggal_peminjaman);
                conSQL.Open();
                command.ExecuteNonQuery();
            }
            catch(Exception e) 
            {
                string err = e.Message;
            }
            finally
            {
                conSQL.Close();
            }
            return Ok();
        }

        // GET: RuanganController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        // POST: RuanganController/Edit/5
        [HttpPut("update/{id}")]
        public ActionResult Edit(int id, [FromBody] Kelas data)
        {
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                string sql = @"update reservasi_kelas set tanggal_peminjaman = @tanggal_peminjaman, waktu_mulai = @waktu_mulai, waktu_selesai = @waktu_selesai, ruangan= @ruangan, satuan_waktu = @satuan_waktu, berapa_kali = @berapa_kali where id = @id;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.AddWithValue("@id", id);                
                command.Parameters.AddWithValue("@ruangan", data.ruangan);
                command.Parameters.AddWithValue("@waktu_mulai", data.waktu_mulai);
                command.Parameters.AddWithValue("@waktu_selesai", data.waktu_selesai);
                command.Parameters.AddWithValue("@satuan_waktu", data.satuan_waktu);
                command.Parameters.AddWithValue("@berapa_kali", data.berapa_kali);               
                command.Parameters.AddWithValue("@tanggal_peminjaman", data.tanggal_peminjaman);
                conSQL.Open();
                command.ExecuteNonQuery();
            }
            catch
            {

            }
            finally
            {
                conSQL.Close();
            }
            return Ok();
        }

        // GET: RuanganController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        // POST: RuanganController/Delete/5
        [HttpDelete("remove/{id}")]
        public ActionResult Delete(int id)
        {
            SqlConnection conSQL = new SqlConnection(_connstring);
            try
            {
                string sql = @"delete from reservasi_kelas WHERE id = @id;";
                SqlCommand command = new SqlCommand(sql, conSQL);
                command.Parameters.AddWithValue("@id", id);
                conSQL.Open();
                command.ExecuteNonQuery();
            }
            catch
            {

            }
            finally
            {
                conSQL.Close();
            }
            return Ok();
        }
    }
}
