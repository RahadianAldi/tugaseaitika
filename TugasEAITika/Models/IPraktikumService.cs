﻿using System.Collections.Generic;
using System.ServiceModel;
using TugasEAITika.Models;

namespace TugasEAITika.Services
{
    [ServiceContract]
    public interface IPraktikumService
    {
        [OperationContract]
        string Speed(string s);

        [OperationContract]
        void XmlMethod(System.Xml.Linq.XElement xml);

        [OperationContract]
        List<Praktikum> ListPraktikum();

        [OperationContract]
        Praktikum DetailPraktikum(int id);
    }
}
