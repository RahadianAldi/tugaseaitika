﻿using System;
using System.Runtime.Serialization;

namespace TugasEAITika.Models
{
    public class Praktikum
    {
        public int id { get; set; }
        public string nama_lengkap { get; set; }
        public string nim { get; set; }
        public string nomor_telpon { get; set; }
        public string email { get; set; }
        public string nama_penanggung_jawab { get; set; }
        public string laboratorium { get; set; }
        public string mata_kuliah { get; set; }
        public string nama_modul { get; set; }
        public string ruangan { get; set; }
        public string waktu_mulai { get; set; }
        public string waktu_selesai { get; set; }
        public string satuan_waktu { get; set; }
        public int berapa_kali { get; set; }
        public string create_by { get; set; }
        public string update_by { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
        public string tanggal_peminjaman { get; set; }
    }

    //[DataContract]
    public class MachineModel
    {
        //[DataMember]
        public int Id { get; set; }

        //[DataMember]
        public string MachineName { get; set; }
        //[DataMember]
        public string HorsePower { get; set; }
    }
}
