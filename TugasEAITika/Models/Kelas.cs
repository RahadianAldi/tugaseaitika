﻿using System;
using System.Collections.Generic;

namespace TugasEAITika.Models
{
    public class Kelas
    {
        public int id { get; set; }
        public string nama_lengkap { get; set; }
        public string nim { get; set; }
        public string nomor_telpon { get; set; }
        public string email { get; set; }
        public string organisasi { get; set; }
        public string jabatan { get; set; }
        public string nama_penanggung_jawab { get; set; }
        public string ruangan { get; set; }
        public string tanggal_peminjaman { get; set; }
        public string waktu_mulai { get; set; }
        public string waktu_selesai { get; set; }
        public string satuan_waktu { get; set; }
        public string berapa_kali { get; set; }
        public string nama_acara { get; set; }
        public string kategori_acara { get; set; }
        public string deskripsi_acara { get; set; }
        public string create_by { get; set; }
        public string update_by { get; set; }
        public DateTime create_date { get; set; }
        public DateTime update_date { get; set; }
    }

    public class IndexData
    {
        public List<Kelas> Kelas { get; set; }
        public List<Praktikum> Praktikum { get; set; }
    }
}
